from tkinter import ttk
import tkinter

class progressBar:
    def __init__(self,max,interval,window):
        """simple progress bar in tkinter"""

        self.window = window                #not really convention but cleaner in main
        self.max = max                      # until prog bar is full
        self.interval = interval            # value of a single step
        self.bar = ttk.Progressbar(window,mode="determinate",
                                   orient=tkinter.HORIZONTAL,length=470,
                                   maximum=self.max)
        self.bar.place(relx=0.17, rely=0.47, relwidth=0.65, relheight=0.0, height=22)

    def increaseBar(self):
        """increase by the set interval"""

        self.bar.step(amount=self.interval)