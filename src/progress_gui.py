import tkinter
from tkinter import ttk
import threading
from . import bar as b
import time
import os

class progress_bar:
    def __init__(self):
        #window settings
        self.window = tkinter.Tk() #this is the root window
        self.window.title('CCDS Distributor List Manager v1.0')
        self.window.configure()
        current_dir=os.path.abspath(os.path.dirname(__file__))
        #icon_path=current_dir+"\\icon.icon"
        #self.window.iconbitmap(icon_path)
        #786x450+650+150
        self.window.geometry("518x200+811+340") #dimensions of the window
        self.window.resizable(False,False)
        #self.creatLookAndFeel()
        self.Heading=tkinter.Label(self.window,text="",font=("Times", 14))
        self.progText = tkinter.Label(self.window,text="",font=("Times", 12))
        self.progText.place(relx=0.0, rely=0.59, height=24, width=545)
        self.Heading.place(relx=0.0, rely=0.3, height=24, width=545)
        #progress bar stuff
        self.bar = b.progressBar(100,1,self.window)
        self.quit_button=ttk.Button(self.window,text="Quit",command=self.window.destroy)
        self.quit_button.place(relx=0.3,rely=0.8,height=30,width=200)
        try:
            threading.Thread(self.funcToRun()).start() #this allows process to run alongside main
            tkinter.mainloop()
        except:
            pass

    def progBar(self):
        """everytime you want to increase the progress bar call this"""

        threading.Thread(target=self.bar.increaseBar).run()  # this snatches the main thread from tkinter
        self.window.update()
    def _quit(self):
        self.window.destroy()
if __name__=='__main__':
    x = progress_bar() #calls the object
    for i in range(100):
        time.sleep(0.1) #represents the work being done
        x.progBar()
        if i < 10:
            x.progText.configure(text="loading...")
        elif i < 50:
            x.progText.configure(text="working on it...")
        elif i < 100:
            x.progText.configure(text="nearly there...")
        elif i==100:
            x._quit()
