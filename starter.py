from src.get_files import select_folder
from subprocess import call,Popen
import os
import re
import pandas as pd
from src.progress_gui import progress_bar
import time
import tkinter as tk
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
import xlwings as xl
def get_bookmarsk(s):
    text=s[s.find("(")+1:s.find(")")]
    list_numbers=re.findall(r'\d+',text)
    if len(list_numbers)==2:
        return list_numbers
    else:
        return None
def clean_data(text,specifc_string):
    #print(specifc_string)
    new_text=""
    new_text=text.replace(specifc_string,"")
    #print(new_text)
    if specifc_string.find("Description")>=0 or specifc_string.find("Type")>=0:
        new_text=new_text.strip(" ")
    else:
        new_text=new_text.replace(" ","")
        #print(specifc_string)
   # print("***********")
    return new_text
class convert_to_text:
    def __init__(self):
        try:
            a=select_folder()
            list_files=a.find_pdf_files()
            list_application_type=[]
            list_product_name=[]
            list_application_number=[]
            list_sequence_number=[]
            list_submission_date=[]
            list_source_file=[]
            list_submission_type=[]
            list_submssion_format=[]
            list_total_bookmars=[]
            list_total_hyperlinks=[]
            list_total_files=[]
            list_total_validations=[]
            count_of_file=0
            status_bar=progress_bar()
            status_bar.Heading.configure(text="Extracting Metric Information from the documents.")
            for FILE in list_files:
                print("########################################")
                submission_format=None
                submission_date=None
                application_number=None
                product_name=None
                application_type=None
                submission_type=None
                sequence_number=None
                total_bookmarks=None
                total_links=None
                Total_files=None
                validation_time=None
                self.FILE=FILE
                print(self.FILE)
                source_file=list_files[FILE]
                #print(self.FILE)
                current_dir=os.path.abspath(os.path.dirname(__file__))
                full_path=current_dir+"/metadata/a.txt"
                status=call(["pdftotext","-layout",self.FILE,full_path])
                #print(status)
                #dict_values={"Submission Format":[],"Dossier Identifier":[],"Do"}
                open_file=None
                read_file=None
                try:
                    open_file=open(full_path,encoding='utf-8')
                    read_file=open_file.read()
                except UnicodeDecodeError:
                    open_file=open(full_path)
                    read_file=open_file.read()
                open_file.close()
                split_the_file=read_file.split("\n")
                count=0
                for i in range(len(split_the_file[:100])):
                    if split_the_file[i].find("Submission Format")>=0:                                  # Submission Format
                        submission_format=clean_data(split_the_file[i],"Submission Format")
                        count=count+1               
                    if split_the_file[i].find("Submission Date")>=0:                                    #Submission Date
                        submission_date=clean_data(split_the_file[i],"Submission Date")
                        year=submission_date[0:4]
                        month=submission_date[4:6]
                        date=submission_date[6:8]
                        calendar={1:"Jan",2:"Feb",3:"Mar",4:"Apr",5:"May",6:"Jun",7:"Jul",8:"Aug",9:"Sep",10:"Oct",11:"Nov",12:"Dec"}
                        submission_date=year+"-"+calendar[int(month)]+"-"+date
                    if split_the_file[i].find("Validation Start Time")>=0:
                        validation_time=clean_data(split_the_file[i],"Validation Start Time")
                    if split_the_file[i].find("Application Number")>=0:                                 # Application Number
                        application_number=clean_data(split_the_file[i],"Application Number")
                        count=count+1
                    if split_the_file[i].find("Dossier Identifier")>=0:                                 # Dossier Identifier
                        application_number=clean_data(split_the_file[i],"Dossier Identifier")
                        count=count+1
                    if split_the_file[i].find("eSubmission Id")>=0:
                        application_number=clean_data(split_the_file[i],"eSubmission Id")
                    if split_the_file[i].find("Product Name")>=0 and split_the_file[i].find("Product Name Type")<0: # Product Name
                        product_name=clean_data(split_the_file[i],"Product Name")                   
                        count=count+1
                    if split_the_file[i].find("Application Type")>=0: 
                        application_type=clean_data(split_the_file[i],"Appl ication Type")
                        count=count+1
                    if split_the_file[i].find("Dossier Type")>=0:
                        application_type=clean_data(split_the_file[i],"Dossier Type")
                        count=count+1
                    if split_the_file[i].find("Submission Type")>=0:
                        submission_type=clean_data(split_the_file[i],"Submission Type")
                        count=count+1
                    if split_the_file[i].find("Sequence Description")>=0:
                        submission_type=clean_data(split_the_file[i],"Sequence Description")
                        count=count+1
                    if split_the_file[i].find("Sequence Number")>=0:
                        sequence_number=clean_data(split_the_file[i],"Sequence Number")
                    if split_the_file[i].find("Total Bookmarks")>=0:
                        #print(1111111111111111111111111111111)
                        get_bookmarks_hyperlinks=get_bookmarsk(split_the_file[i])
                        bookmarsks=get_bookmarks_hyperlinks[0]
                        hyperlinks=get_bookmarks_hyperlinks[1]
                        #print("Booksmarks",bookmarsks)
                        #print("hyperlinks",hyperlinks)
                        total_bookmarks=int(bookmarsks)
                        total_links=int(hyperlinks)
                        #print(22222222222222222222)
                        count=count+1
                    if split_the_file[i].find("Total")>=0 and split_the_file[i].find("Total Bookmarks")<0 and len(split_the_file[i])>11 and split_the_file[i].find("B")>=0:
                        extract_number=re.findall(r'\d+',split_the_file[i])
                        #print(extract_number)
                        count=count+1
                        #Total_files=clean_data(split_the_file[i],"Total")
                        Total_files=int(extract_number[1])
                        print(Total_files)
                #print(Total_files,application_number,application_type)
                print(validation_time)
                list_application_type.append(application_type)
                list_product_name.append(product_name)
                list_application_number.append(application_number)
                list_sequence_number.append(sequence_number)
                list_submission_date.append(submission_date)
                list_source_file.append(source_file)
                list_submission_type.append(submission_type)
                list_submssion_format.append(submission_format)
                list_total_bookmars.append(total_bookmarks)
                list_total_hyperlinks.append(total_links)
                list_total_files.append(Total_files)
                list_total_validations.append(validation_time)
                count_of_file=count_of_file+1
                time.sleep(0.1)
                status_bar.progBar()
                status_bar.progText.configure(text=str(count_of_file)+" "+source_file)
            status_bar._quit()
            #print(list_total_files)
            root=tk.Tk()
            root.withdraw()
            root.filename =  filedialog.asksaveasfilename(initialdir = current_dir,title = "Select file",filetypes = (("Excel Files","*.xlsx"),("all files","*.*")))
            file_name=root.filename
#            root=None
            dict_values={"Application Type/Dossier Type":list_application_type,"Product Name":list_product_name,"Application Number/ Dossier Identifier":list_application_number,"Sequence Number":list_sequence_number,"Submission Date":list_submission_date,"Validation Time":list_total_validations,"Source File Nmae":list_source_file,"Submission Type/Sequence Description":list_submission_type,"Submission Format":list_submssion_format,"Total Bookmarks":list_total_bookmars,"Total Hyperlinks":list_total_hyperlinks,"Total Files":list_total_files}
            #print(dict_values)
            df = pd.DataFrame(dict_values)
            writer=pd.ExcelWriter(file_name,engine="xlsxwriter")
            df.to_excel(writer,sheet_name="Metrics",index=False)
            writer.save()
            print(file_name)
            try:
                app=xl.App(visible=False)
                book=xl.Book(file_name)
                Metrics_sheet=book.sheets[0]
                list_of_empty_values=[]
                total_rows=len(Metrics_sheet.range('E2').expand("down").value)
                #list_of_values=Metrics_sheet.range('A2:K'+str(total_rows)).value
                _cleaning_bar=progress_bar()
                _cleaning_bar.Heading.configure(text="Cleaning rows")
                for i in range(2,total_rows+2):
                    print(i)
                    print(Metrics_sheet.range('J'+str(i)))
                    if Metrics_sheet.range('A'+str(i)).value==None or Metrics_sheet.range('B'+str(i)).value==None or Metrics_sheet.range('C'+str(i)).value==None or Metrics_sheet.range('D'+str(i)).value==None or Metrics_sheet.range('E'+str(i)).value==None or Metrics_sheet.range('G'+str(i)).value==None or Metrics_sheet.range('H'+str(i)).value==None or Metrics_sheet.range('I'+str(i)).value==None or Metrics_sheet.range('J'+str(i)).value==None or Metrics_sheet.range('K'+str(i)).value==None:
                        Metrics_sheet.range('A'+str(i)+":L"+str(i)).color=(255,192,203)
                    time.sleep(0.1)
                    _cleaning_bar.progBar()
                    _cleaning_bar.progText.configure(text="Row number: "+str(i))
                _cleaning_bar._quit()
                Metrics_sheet.autofit()
            except PermissionError:
                root=tk.Tk()
                root.withdraw()
                messagebox.showerror("Error","Please Close the Excel File")
                #root=None
            finally:
                book.save()
                book.close()
                messagebox.showinfo("Result","Completed")
                root=None
        except Exception as error:
            print(error)
if __name__=="__main__":
    convert_to_text()